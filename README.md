
Use: GitLab Geo when using OpenShift on AWS with managed postgres via RDS

# Pre-Requisites:
- 1 RDS instance in `us-east-1`
	- Enable read-replica instance in `us-west-2` (Creates a new instance in us-west-2)
- 1 RDS instance in `us-west-2`
	- Serves as the instance for the geo database required on the secondary instance


Total of 3 RDS instances are created

# Database pre-work


## Create RDS Instances:

### UI Steps

### AWS CLI Steps


## Create postgres databases on RDS Instances:

### Primary Database (Replicates to Read Replica Instance Automatically)
`psql -d "host=primary_url port=5432 dbname=postgres user=postgres password=gitlab123"`

`CREATE database gitlabhq_geo_production;`
`CREATE database gitlabhq_production;`

### Secondary Geo Database
`psql -d "host=gitlab-geo-secondary.cxex4g0ubkuu.us-west-2.rds.amazonaws.com port=5432 dbname=postgres user=postgres password=gitlab123"`

`CREATE database gitlabhq_geo_production;`

# Create OpenShift Clusters
```
rosa create cluster --cluster-name webdog --sts --mode auto --yes --region us-east-1
rosa create cluster --cluster-name webdog-ro --sts --mode auto --yes --region us-west-2
```

# Create cluster-admin users on both clusters

```shell
rosa create admin -c webdog

oc login https://api.webdog.jxnh.p1.openshiftapps.com:6443 --username cluster-admin --password gk7mr-giLsV-Gy3kd-xjcss
```


```shell
rosa create admin -c webdog-ro

oc login https://api.webdog-ro.0kok.p1.openshiftapps.com:6443 --username cluster-admin --password zD2U3-BCI8M-A3PtR-BAXRj
```

# webdog cluster

## Login to oc to get kubectl access
	`oc login https://api.webdog.jxnh.p1.openshiftapps.com:6443 --username cluster-admin --password gk7mr-giLsV-Gy3kd-xjcss`

## Login to Console
`rosa describe cluster --cluster=webdog -o json | jq -r '.console.url' | pbcopy

`https://console-openshift-console.apps.webdog.et1j.p1.openshiftapps.com`

## Operator Installation
- `cert-manager` 

Create `ClusterIssuer-Primary.yaml`

```yaml
---
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-prod
spec:
  acme:
    email: <your-email>
    # User is not responsible for privateKeySecretRef, but is required to be provided.
    privateKeySecretRef:
      name: letsencrypt-prod
    server: https://acme-v02.api.letsencrypt.org/directory
    solvers:
      - http01:
          ingress:
            class: nginx
      - selector:
          dnsZones:
            - "*.apps.webdog.jxnh.openshiftapps.com" # Wildcard certificate for the OpenShift Cluster
        dns01:
          route53:
            region: us-east-1
            accessKeyID: <your-aws-access-key-id>
            secretAccessKeySecretRef:
              name: prod-route53-credentials-secret
              key: secret-access-key

```


- `nginx-ingress-controller` via Operator Hub

## Install GitLab Operator CRDs and RDS password
`bash operator.sh`

## Create AWS Secret to update route53/Let's Encrypt automatically with certificates
`kubectl -n gitlab-system create secret generic prod-route53-credentials-secret --from-literal=secret-access-key=<your-secret-access-key`

## Add Primary Domain to Primary YAML file

`PRIMARY_DOMAIN=$(rosa describe cluster --cluster=webdog -o json | jq -r '.console.url' | egrep -o "apps.*")`
`GITLAB_URL=gitlab.$PRIMARY_DOMAIN
`gitlab-ee-geo-primary.yaml`:
	ADD `$PRIMARY_DOMAIN` to `spec.chart.values.global.hosts.domain` and `spec.chart.values.global.geo.nodeName` stanzas


## Deploy GitLab Primary Instance
`kubectl -n gitlab-system apply -f gitlab-ee-geo-primary.yaml`

`kubectl -n gitlab-system logs deployment/gitlab-controller-manager -c manager -f`

## Retrieve Primary Secrets
`bash secrets.yaml`

## Copy root password for GitLab Login
`kubectl -n gitlab-system get secret gitlab-gitlab-initial-root-password -ojsonpath='{.data.password}' | base64 -d | pbcopy`

# Enable Geo on Primary Cluster
## Get toolbox pod
`GL_TOOLBOX=$(kubectl -n gitlab-system get pods | grep -i toolbox | awk {'print $1}')`

## Set the primary  as primary node for Geo
`kubectl --namespace gitlab-system exec -ti $GL_TOOLBOX -c toolbox -- gitlab-rake geo:set_primary_node`

## Set the Internal URL of the Primary node

`kubectl --namespace gitlab-system exec -ti $GL_TOOLBOX -c toolbox -- gitlab-rails runner "GeoNode.primary_node.update!(internal_url: ``'https://$GITLAB_URL/')"


## Check the status of Geo
`kubectl --namespace gitlab-system exec -ti $GL_TOOLBOX -c toolbox -- gitlab-rake gitlab:geo:check`

# webdog-ro cluster
`rosa create admin -c webdog-ro`

`oc login https://api.webdog-ro.6ios.p1.openshiftapps.com:6443 --username cluster-admin --password eXRdm-CFRoq-4IfEi-Dwadk`

Admin Console: `https://console-openshift-console.apps.webdog-ro.6ios.p1.openshiftapps.com`


# Route 53 GitLab Configuration

## OpenShift Primary Cluster

`GL_INGRESS_URL=$(kubectl -n gitlab-system get svc | grep -i gitlab)`

### Add CNAME Record to OpenShift Primary PUBLIC Hosted Zone:
`gitlab.apps.webdog.et1j.p1.openshift.apps.com` -> `$GL_INGRESS_URL`

### Add CNAME Record to OpenShift Primary PRIVATE Hosted Zone

`gitlab.apps.webdog.et1j.p1.openshiftapps.com` -> `$GL_INGRESS_URL`

## OpenShift Secondary Cluster
- Switch clusters for kubectl
`oc login to secondary cluster`

*Repeat Steps Above for Public and Private Hosted Zones*


# Enable Geo on Primary Instance



# Enable Geo on Secondary Site
## Login to GitLab Primary Instance
## Sign into Admin Console
## Navigate to Geo
## Click Add Site
## Add Internal/External URL